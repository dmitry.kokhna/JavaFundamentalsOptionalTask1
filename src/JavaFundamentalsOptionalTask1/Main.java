package JavaFundamentalsOptionalTask1;

//Найти самое короткое и самое длинное число. Вывести найденные числа и их длину.

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    //метод подсчета длинны
    public static int countOfNumbers(int number) {
        int lengthOfNumber = 0;
        long tempForCount = 1;
        while (tempForCount <= number) {
            lengthOfNumber++;
            tempForCount = tempForCount * 10;
        }
        return lengthOfNumber;
    }
    public static void main(String[] args) {
        int[] arrayCount = new int[4];
        Scanner scannerForNumbers = new Scanner(System.in);
        System.out.println("Вам нужно ввести  положительных 4 числа");

        System.out.println("Введите первое число");

        int firstNumber = Math.abs(scannerForNumbers.nextInt());
        int firstOfCount = countOfNumbers(firstNumber);
        arrayCount[0] = firstOfCount;

        System.out.println("Введите второе число");
        int secondNumber = Math.abs(scannerForNumbers.nextInt());
        int secondOfCount = countOfNumbers(secondNumber);
        arrayCount[1] = secondOfCount;

        System.out.println("Введите третье число");
        int thirdNumber = Math.abs(scannerForNumbers.nextInt());
        int thirdOfCount = countOfNumbers(thirdNumber);
        arrayCount[2] = thirdOfCount;

        System.out.println("Введите четверное число");
        int fourNumber = Math.abs(scannerForNumbers.nextInt());
        int fourOfCount = countOfNumbers(fourNumber);
        arrayCount[3] = fourOfCount;


        //проверка , чтобы числа не были равной длинны
        Arrays.sort(arrayCount);
        int x = 0;
        for (int i = 0; i < arrayCount.length-1; i++) {
           int doubles=0;
            if (arrayCount[i] == arrayCount[i + 1]) {
               doubles++;
                x=doubles;
                            }
        }
        if(x>0){
            System.out.println("Вы ввели числа одинаковой длинны, повторите ввод всех чисел");

        } else {

            System.out.println("Ввод выполнен");

            if ((firstOfCount > secondOfCount) && (firstOfCount > thirdOfCount) && (firstNumber > fourNumber)) {
                System.out.println("Наибольшая длина у числа " + firstNumber + " его длина " + firstOfCount + " символов");
            } else if ((secondOfCount > thirdOfCount) && (secondOfCount > fourOfCount)) {
                System.out.println("Наибольшая длина у числа " + secondNumber + " его длина " + secondOfCount + " символов");
            } else if ((thirdNumber > fourNumber)) {
                 System.out.println("Наибольшая длина у числа " + thirdNumber + " его длина " + thirdOfCount + " символов");
            } else {
                 System.out.println("Наибольшая длина у числа " + fourNumber+ " его длина " + fourOfCount+ " символов");
            }

            if ((firstOfCount < secondOfCount) && (firstOfCount < thirdOfCount) && (firstNumber < fourNumber)) {
                System.out.println("Наименьшая длина у числа " + firstNumber + " его длина " + firstOfCount + " символов");
            } else if ((secondOfCount < thirdOfCount) && (secondOfCount < fourOfCount)) {
                System.out.println("Наименьшая длина у числа " + secondNumber + " его длина " + secondOfCount + " символов");
            } else if ((thirdNumber < fourNumber)) {
                System.out.println("Наименьшая длина у числа " + thirdNumber + " его длина " + thirdOfCount + " символов");
            } else {
                System.out.println("Наименьшая длина у числа " + fourNumber + " его длина " + fourOfCount+ " символов");
            }
        }
        }
            }
